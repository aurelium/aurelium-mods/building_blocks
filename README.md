I couldn't find a license for the original code of this mod.  The URL and a link
to the original repo can be found here: 
https://forum.minetest.net/viewtopic.php?id=674
Unfortunately the original repository is now defunct.

I'm adding the GPLv3 to this project, just for good measure.  But the copyright
and licensing should be referred to the original author: sdzen